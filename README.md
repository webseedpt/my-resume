# MyResume

[![Build Status](https://travis-ci.org/ricardomfmsousa/my-resume.svg?branch=master)](https://travis-ci.org/ricardomfmsousa/my-resume)
[![Coverage Status](https://coveralls.io/repos/github/ricardomfmsousa/my-resume/badge.svg?branch=master)](https://coveralls.io/github/ricardomfmsousa/my-resume?branch=master)
[![dependencies Status](https://david-dm.org/ricardomfmsousa/my-resume/status.svg)](https://david-dm.org/ricardomfmsousa/my-resume)
[![devDependencies Status](https://david-dm.org/ricardomfmsousa/my-resume/dev-status.svg)](https://david-dm.org/ricardomfmsousa/my-resume?type=dev)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.9.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Deploy
Run `yarn deploy` to automatically deploy the project to github pages.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
